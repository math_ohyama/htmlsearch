$(function() {
// jsRenderのデバッグモード
	$.views.debugMode = false;

	$.views.converters("daymonth", function(val) {
		return moment(val, 'YYYYMMDD').format("YYYY年MM月DD日");
	});
	$.views.converters("score", function(val) {
		return val.toFixed(3);
	});
	$.views.converters("title20", function(val) {
		return val.substring(0, 20);
	});

	$('a[rel=popover]').click(function() {
		$('a[rel=popoever]').not(this).popover('hide');
	});
	$('form').on('submit', function(e) {
		e.preventDefault();
		e.stopPropagation();
		if ($("#keyword").val() == "") {
			alert("検索文字を入力してください");
			return false;
		}
		$.ajax({
			type: 'GET',
			url: 'find.php',
			async: true,
			dataType: 'json',
			data: $(this).serializeArray(),
			beforeSend: function() {
				blockUI();
			},
			success:
							function(json, dataType) {
								if (json === null || json.length === 0) {
									alert("検索結果は0件です");
								} else {
									$("#searchResult").html(
													$('#searchResultTmpl').render(json)
													);
									$('a[rel=popover]').popover().click(function(e) {
										$('a[rel=popover]').not(this).popover('hide');
									});
								}
							},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert('Error:' + errorThrown);
			},
			complete: function() {
				$.unblockUI();
			}
		});
	});

	function blockUI() {
		$.blockUI({
			message: '<p><img src="./images/AjaxLoader.gif" style="vertical-align:middle;" /></p>',
			css: {
				width: '70px',
				border: 'none',
				padding: '10px',
				left: '50%',
				backgroundColor: '#FFF'
			}
		});
	}
});
