<?php

set_include_path(join(PATH_SEPARATOR, array(
		realpath(__DIR__ . '/phplib'),
		get_include_path(),
)));

define('LOG_FILE_NAME', 'news-search-#DT#.log'); //ログファイル名
define('APP_DIR', __DIR__);

require_once 'Zend/Loader/Autoloader.php';
require_once 'logger.php';
require_once 'functions.php';

$env = (isset($_SERVER['APP_ENV']) ? $_SERVER['APP_ENV'] : 'development');
require_once 'profile' . DIRECTORY_SEPARATOR . $env . DIRECTORY_SEPARATOR . 'config.php';


Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

setlocale(LC_ALL, 'ja_JP.UTF-8');

$indexDir = realpath(APP_DIR . '/index');
//
// Zend_Search_Lucene_Analysis_Analyzer::setDefault(new Phpbean_Lucene_Analyzer());
//文字列解析オブジェクトをUTF8を受け付けるものに設定
$analyzer = new JaAnalyzer();
//$analyzer = new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8();
//次の単語は無視
$stopWords = array('SIZE', 'they', 'those', 'that', 'NONE', 'タイトルナシ');
$stopWordsFilter =
				new Zend_Search_Lucene_Analysis_TokenFilter_StopWords($stopWords);
$analyzer->addFilter($stopWordsFilter);

//長さの短かい単語は無視
$shortWordsFilter =
				new Zend_Search_Lucene_Analysis_TokenFilter_ShortWords(3);
$analyzer->addFilter($shortWordsFilter);

//この文字列解析オブジェクトを利用するように設定
Zend_Search_Lucene_Analysis_Analyzer::setDefault($analyzer);
