<?php

//include_pathは適切に設定されている前提で...
require_once('Zend/Log.php');
require_once('Zend/Log/Writer/Stream.php');


/**
 * log writer
 * @param  String    $message  エラーメッセージ
 * @param  String    $level    ログレベル
 * @see    Zend/Log.php
 */
function logWrite($message, $level = Zend_Log::INFO) {
	//'#DT#'を日付に差替える。日毎にログを分割（1ヶ月でローテート）する。
	$logFile = LOG_PATH . '/' . strtr(LOG_FILE_NAME, array('#DT#' => strftime('%d')));
	$logger = new Zend_Log(new Zend_Log_Writer_Stream($logFile));
	$logger->log($message, $level);
}

//ログレベル
/*
EMERG   = 0;  // 緊急事態 (Emergency): システムが使用不可能です
ALERT   = 1;  // 警報 (Alert): 至急対応が必要です
CRIT    = 2;  // 危機 (Critical): 危機的な状況です
ERR     = 3;  // エラー (Error): エラーが発生しました
WARN    = 4;  // 警告 (Warning): 警告が発生しました
NOTICE  = 5;  // 注意 (Notice): 通常動作ですが、注意すべき状況です
INFO    = 6;  // 情報 (Informational): 情報メッセージ
DEBUG   = 7;  // デバッグ (Debug): デバッグメッセージ
*/

//エラーでログ出力
//logWrite("エラーです。", Zend_Log::ERR);

//デバッグでログ出力
// logWrite("デバッグ情報です。", Zend_Log::DEBUG);
