<?php

class StringUtil {

	public static function endsWith($haystack, $needle) {
		$length = (strlen($haystack) - strlen($needle));
		if ($length < 0)
			return false;
		return strpos($haystack, $needle, $length) !== false;
	}

	/**
	 * 文字列が特定の接頭辞から始まるか
	 *
	 * @param string $str 文字列
	 * @param string $prefix 接頭辞
	 * @return boolean
	 */
	public static function startsWith($str, $prefix) {
		return (strpos($str, $prefix, 0) === 0) ? true : false;
	}

	/*
	  $str = 'abcdefg';
	  startswith($str, 'abc'); //true
	  startswith($str, 'foo'); //false
	  endswith($str, 'efg');   //true
	  endswith($str, 'bar');   //false
	 */
}