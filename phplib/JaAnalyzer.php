<?php

/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Search_Lucene
 * @subpackage Analysis
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Utf8.php 24593 2012-01-05 20:35:02Z matthew $
 */
/** Zend_Search_Lucene_Analysis_Analyzer_Common */
require_once 'Zend/Search/Lucene/Analysis/Analyzer/Common/Utf8.php';

/**
 * @category   Zend
 * @package    Zend_Search_Lucene
 * @subpackage Analysis
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class JaAnalyzer extends Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8 {

	/**
	 * Current char position in an UTF-8 stream
	 *
	 * @var integer
	 */
	private $_position;

	/**
	 * Current binary position in an UTF-8 stream
	 *
	 * @var integer
	 */
	private $_bytePosition;

	/**
	 * Reset token stream
	 */
	public function reset() {
		$this->_position = 0;
		$this->_bytePosition = 0;

		// convert input into UTF-8
		if (strcasecmp($this->_encoding, 'utf8') != 0 &&
						strcasecmp($this->_encoding, 'utf-8') != 0) {
			$this->_input = iconv($this->_encoding, 'UTF-8', $this->_input);
			$this->_encoding = 'UTF-8';
		}
	}

	/**
	 * Tokenization stream API
	 * Get next token
	 * Returns null at the end of stream
	 *
	 * @return Zend_Search_Lucene_Analysis_Token|null
	 */
	public function nextToken() {
		if ($this->_input === null) {
			return null;
		}

		do {
			$bigram = false;
			$bigram_first_letter = null;
			$matchedWord = null;
			if (preg_match('/[\p{Ll}\{Lu}]+/u', $this->_input, $match, PREG_OFFSET_CAPTURE, $this->_bytePosition)) {
				$matchedWord = $match[0][0];
				//ascii charater
			} else if (preg_match('/([\p{L}])([\p{L}])/u', $this->_input, $match, PREG_OFFSET_CAPTURE, $this->_bytePosition)) {
				$bigram = true;
				$matchedWord = $match[0][0];
				$bigram_first_letter = $match[1][0];
				//two characters
			} else {
				// It covers both cases a) there are no matches (preg_match(...) === 0)
				// b) error occured (preg_match(...) === FALSE)
				return null;
			}

			// matched string
			$matchedWord = $match[0][0];
			//print $matchedWord."\n";
			// binary position of the matched word in the input stream
			$binStartPos = $match[0][1];

			// character position of the matched word in the input stream
			$startPos = $this->_position +
							iconv_strlen(substr($this->_input, $this->_bytePosition, $binStartPos - $this->_bytePosition), 'UTF-8');
			// character postion of the end of matched word in the input stream
			$endPos = $startPos + iconv_strlen($matchedWord, 'UTF-8');
			$token = $this->normalize(new Zend_Search_Lucene_Analysis_Token($matchedWord, $startPos, $endPos));

			if (!$bigram) {
				$this->_bytePosition = $binStartPos + strlen($matchedWord);
				$this->_position = $endPos;
			} else {
				$this->_bytePosition = $binStartPos + strlen($bigram_first_letter);
				$this->_position = $startPos + iconv_strlen($bigram_first_letter, 'UTF-8');
			}
		} while ($token === null); // try again if token is skippedd

		return $token;
	}

}

