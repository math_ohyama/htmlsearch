<!DOCTYPE html>
<?php
require_once __DIR__ . '/init.php';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';
$index = Zend_Search_Lucene::open($indexDir);

$total = $index->numDocs();
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>全文検索</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet/less" href="bower_components/bootstrap/less/bootstrap.less">
		<link rel="stylesheet/less" href="bower_components/bootstrap/less/responsive.less">
		<link rel="stylesheet" href="style.css">
		<script src="bower_components/less/dist/less-1.4.2.min.js"></script>
		<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="bower_components/html5shiv/dist/html5shiv.js"></script>
    <![endif]-->
		<style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="#">大学ニュース全文検索</a>
					<p class="navbar-text pull-right">
						<?php echo $total ?>文書登録されています
					</p>
					<div class="nav-collapse collapse">
            <ul class="nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">使い方<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#"><i class="icon-font" style="color:red"></i>：PDFファイル</a></li>
									<li><a href="#"><i class="icon-file"></i>：HTMLファイル</a></li>
									<li><a href="#">キーワードをスペースで区切って検索してください。<br/>
											<li><a href="#"><strong>AND</strong>：複数キーワードを入力した場合に、各々のキーワードが含まれる文章を検索します</a></li>
											<li><a href="#"><strong>OR</strong>：複数キーワードを入力した場合に、いずれかのキーワードが含まれる文章を検索します</a></li>
										</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>


		<div class="container">
      <div class="jumbotron">
				<form class="form-search form-horizontal">
					<div class="control-group">
						<label class="radio inline">
							<input type="radio" name="type" id="type" value="pdf"><i class="icon-font" style="color:red"></i>PDF
						</label>
						<label class="radio inline">
							<input type="radio" name="type" id="type" value="html"><i class="icon-file"></i>HTML
						</label>
						<label class="radio inline">
							<input type="radio" name="type" id="type" value="both" checked="checked">両方
						</label>
					</div>
					<div class="control-group">
						<label class="radio inline">
							<input type="radio" name="operation" id="operation" value="and" checked="checked">AND
						</label>
						<label class="radio inline">
							<input type="radio" name="operation" id="operation" value="or">OR
						</label>
					</div>
					<div class="control-group">
						<div class="input-append">
							<input id="keyword" type="text" name="keyword" class="input-large search-query" value="" placeholder="キーワードを入力してください"/>
							<button class='btn add-on'>
								<i class="icon-search"></i>
							</button>
						</div>
					</div>
				</form>
      </div>

      <hr>
			<div id="result">
				<table class="table table-bordered table-striped">
					<thead>
						<tr><th>#</th><th>Score</th><th>タイトル</th></tr>
					</thead>
					<tbody id="searchResult">
					</tbody>
				</table>
			</div>
      <!--
      <div class="footer">
        <p>&copy; StudioEgg k.k. 2013</p>
      </div>
      -->
			<script id="searchResultTmpl" type="text/x-jsrender">
				<tr>
				<td>
				{{if type == 'pdf'}}
				<i class="icon-font" style="color:red"></i>
				{{else}}
				<i class="icon-file"></i>
				{{/if}}&nbsp;{{:#index}}</td>
				<td>{{score:score}}</td>
				<td><a href="{{url:url}}" target="_blank"
				rel="popover"
				data-title="詳細情報"
				data-html="true"
				data-trigger="hover"
				data-content="&lt;div class=&quot;meta-detail&quot;&gt;&lt;b&gt;ID&lt;/b&gt;：{{:id}}&lt;br/&gt;&lt;b&gt;ファイル名&lt;/b&gt;：{{:filename}}&lt;br/&gt;&lt;b&gt;作成日&lt;/b&gt;：{{daymonth:created}}&lt;br/&gt;&lt;b&gt;更新日&lt;/b&gt;：{{daymonth:updated}}&lt;br/&gt;&lt;/div&gt;"
				data-animation="true"
				data-placement="right"
				>{{:title}}</a>
				</td>
				</tr>
			</script>
    </div> <!-- /container -->
		<script src="bower_components/jquery/jquery.js"></script>
		<script src="bower_components/jsrender/jsrender.min.js"></script>
		<script src="bower_components/blockui/jquery.blockUI.js"></script>
		<script src="bower_components/momentjs/min/moment.min.js"></script>
		<script src="bower_components/momentjs/min/langs.min.js"></script>
		<script src="bower_components/bootstrap/js/bootstrap-tooltip.js"></script>
		<script src="bower_components/bootstrap/js/bootstrap-dropdown.js"></script>
		<script src="bower_components/bootstrap/js/bootstrap-popover.js"></script>
		<script src="lucene.js"></script>
		<?php
		if ($keyword) {
			?>
			<script type="text/javascript">
				$.ajax({
				type: 'GET',
				url: 'find.php',
				async: true,
				dataType: 'json',
				data: 'keyword=<?php echo urlencode($_GET['keyword']) ?>',
				success: function(json, dataType) {
				if (json === null || json.length === 0) {
				alert("検索結果は0件です");
				} else {
				$("#searchResult").html(
				$('#searchResultTmpl').render(json)
				);
				$('a[rel=popover]').popover();
				}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert('Error:' + errorThrown);
				}
				});
				$('#keyword').val('<?php echo $_GET['keyword'] ?>');
			</script>
		<?php } ?>
	</body>
</html>
