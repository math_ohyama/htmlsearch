<?php

/*
  if (
  !(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') && (!empty($_SERVER['SCRIPT_FILENAME']) && 'find.php' === basename($_SERVER['SCRIPT_FILENAME']))
  ) {
  header('Content-Type: text/html; charset=utf-8');
  die('このページは直接ロードしないで下さい。');
  }
 *
 */

require_once __DIR__ . '/init.php';

$index = Zend_Search_Lucene::open($indexDir);
/*
  $searchKeys = preg_split('/[\s|\x{3000}]+/u', trim($keyword));
  if (count($searchKeys) > 1) {
  $query = new Zend_Search_Lucene_Search_Query_MultiTerm();
  foreach ($searchKeys as $searchKey) {
  $term1 = new Zend_Search_Lucene_Index_Term(trim($searchKey));
  $query->addTerm($term1);
  }
  Zend_Search_Lucene::setResultSetLimit(100);
  $hits = $index->find($query);
  } else {
  $term = new Zend_Search_Lucene_Index_Term(trim($keyword));
  $query = new Zend_Search_Lucene_Search_Query_Term($term, "UTF-8");
  Zend_Search_Lucene::setResultSetLimit(100);
  $hits = $index->find($query);
  }
 */
//全角スペースを半角に変換する
$keyword = mb_convert_kana(isset($_GET['keyword']) ? $_GET['keyword'] : '', 's', 'UTF-8');
$keywords = explode(" ", $keyword);
$operation = strtoupper(isset($_GET['operation']) ? $_GET['operation'] : 'AND');
$type = strtolower(isset($_GET['type']) ? $_GET['type'] : 'both');

foreach ($keywords as $word) {
	if ($word === end($keywords)) {
		$ret .="( title:" . $word . " OR content:" . $word . " ) ";
	} else {
		$ret .="( title:" . $word . " OR content:" . $word . " ) " . $operation . " ";
	}
}
if ($type != 'both') {
	$ret .= " AND type:" . $type;
}
//$query = Zend_Search_Lucene_Search_QueryParser::parse($keyword, 'utf-8');
$query = Zend_Search_Lucene_Search_QueryParser::parse($keyword, 'utf-8');
Zend_Search_Lucene::setResultSetLimit(100);
logWrite($ret);
$hits = $index->find($ret);

if ($index->count()) {
	$count = 0;
	foreach ($hits as $hit) {
		$names = $hit->getDocument()->getFieldNames();
		$result[$count]["id"] = $hit->id;
		$result[$count]["score"] = $hit->score;
		if (in_array('title', $names)) {
			$result[$count]["title"] = $query->htmlFragmentHighlightMatches($hit->title);
		}
		$result[$count]["url"] = $hit->url;
		$result[$count]["filename"] = $hit->filename;
		if (in_array('created', $names)) {
			$result[$count]["created"] = $hit->created;
		}
		if (in_array('updated', $names)) {
			$result[$count]["updated"] = $hit->updated;
		}
		$result[$count]["type"] = $hit->type;
		if (in_array('keyword', $names)) {
			$result[$count]["keyword"] = $hit->keywords;
		}
		$count++;
	}
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($result);
